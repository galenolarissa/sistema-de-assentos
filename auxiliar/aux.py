import os, ast
import random

def visualizaAssentos(thread_id, assentos):
    operation = 1
    pass


def alocaAssentoLivre(thread_id, assento):
    operation = 2
    if valida_assentos[assento] == 0:  # esta livre
        valida_assentos[assento] = thread_id
        return True  # consegui alocar o assento
    else:
        return False  # Assento ocupado


def alocaAssentoDado(thread_id, assento):
    operation = 3
    if valida_assentos[assento] == 0:
        valida_assentos[assento] = thread_id
        return True
    else:
        return False


def liberaAssento(thread_id, assento):
    operation = 4
    if valida_assentos[assento] == thread_id:  # posso liberar
        valida_assentos[assento] = 0
        return True
    else:
        return False


def comparaFinal(valida_assentor, lines):

    if (int) (lines[len(lines) -1][0]) is 1:
        j = 7
        for i in range(n):
            if valida_assentos[i] is (int) (lines[len(lines) -1][j]):
                pass
            else:
                return False
            j = j+3

    else:
        j = 10
        for i in range(n):
            if valida_assentos[i] is (int) (lines[len(lines) - 1][j]):
                pass
            else:
                return False
            j = j+3
        return True


# Main#

nomeArq = raw_input("Entre com o nome do arquivo: ");
file = open(nomeArq, "r")
lines = [x for x in file.readlines()]
# for line in lines:
#     options.append(list(ast.literal_eval(line)))

n = 5
valida_assentos = [0] * (n)  # ignorar primeira posicao

for i in range(len(lines)):

    if (int)(lines[i][0]) is 1:
        visualizaAssentos(int(lines[i][3]), valida_assentos)

    elif (int)(lines[i][0]) is 2:
        # aloca assento livre
        alocaAssentoLivre((int)(lines[i][3]), (int)(lines[i][6]))

    elif (int)(lines[i][0]) is 3:
        # aloca assento dado
        alocaAssentoDado((int)(lines[i][3]), (int)(lines[i][6]))

    elif (int)(lines[i][0]) is 4:
        # libera assento
        liberaAssento((int)(lines[i][3]), (int)(lines[i][6]))

print(valida_assentos)

if comparaFinal(valida_assentos, lines) == False:
    print("O programa nao esta funcionando da forma correta")
else:
    print("Sucesso, o programa esta funcionado")
