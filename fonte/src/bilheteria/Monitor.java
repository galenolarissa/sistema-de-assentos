package bilheteria;

public class Monitor {
  boolean escrevendo;
  int tamanhoFilaEscritores;
  int numeroLeitores;

  public Monitor() {
    this.escrevendo = false;
    this.tamanhoFilaEscritores = 0;
    this.numeroLeitores = 0;
  }

  public synchronized void EntraLeitor() throws InterruptedException {

    while(this.tamanhoFilaEscritores > 0 || this.escrevendo) {
      wait();
    }

    this.numeroLeitores++;

  }

  public synchronized void SaiLeitor() {

    this.numeroLeitores--;

    if(this.numeroLeitores == 0){
        notifyAll();
    }

  }

  public synchronized void EntraEscritor() throws InterruptedException {

    this.tamanhoFilaEscritores++;

    while(this.escrevendo || this.numeroLeitores > 0) {
      wait();
    }

    this.escrevendo = true;
    this.tamanhoFilaEscritores--;

  }

  public synchronized void SaiEscritor() {

    this.escrevendo = false;

    notifyAll();

  }

}
