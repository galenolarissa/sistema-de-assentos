package bilheteria;

import java.io.*;

public class Main {
	public static int tamanhoBuffer = 5;
	public static String[] buffer = new String[tamanhoBuffer];

	public static void main(String[] args) throws FileNotFoundException, UnsupportedEncodingException, InterruptedException {
		String arquivo = "";
		String entrada = "";
		int numeroDeAssentos = 0;

		try{

			InputStreamReader inputReader = new InputStreamReader(System.in);
			BufferedReader br = new BufferedReader(inputReader);
			
			System.out.println("Insira o nome que deseja para o arquivo de log");
			arquivo = br.readLine();
			
			System.out.println("Insira a quantidade de assentos desejada");
			entrada = br.readLine();
			numeroDeAssentos = Integer.parseInt(entrada);
			
		}
		catch(NumberFormatException nfe) {
			System.out.println("Erro ao iniciar o programa. Número de assentos não informado");
			System.exit(0);
		}
		catch(IOException ioe) {
			System.out.println("IO exception rised");
		}
		
		int totalUsuarios = 4;
		Monitor monitor = new Monitor();
		MonitorBuffer monitorBuffer = new MonitorBuffer(tamanhoBuffer);
		Assento assentos = new Assento(numeroDeAssentos);
		Usuario[] usuarios = new Usuario[totalUsuarios];		
		Consumidora consumidora = new Consumidora(buffer, tamanhoBuffer, arquivo, monitorBuffer);
		
		assentos.VisualizaAssento();
		
		for(int i = 0; i < totalUsuarios; i++) {
			usuarios[i] = new Usuario(monitor, monitorBuffer, assentos, i+1);
		}
				
		for(int i = 0; i < totalUsuarios; i++) {
			usuarios[i].start();			
		}
		consumidora.start();

		for(int i = 0; i < totalUsuarios; i++) {
			usuarios[i].join();
		}


		System.out.println("Imprimindo o buffer");
		for (int i = 0; i < tamanhoBuffer; i++) {
			System.out.println(buffer[i]);
		}


		System.exit(0);
	}
}
