package bilheteria;

public class BoundedSemaphore {
	private int signals = 0;
	private int bound = 0;
	
	public BoundedSemaphore(int upperBound) {
		this.bound = upperBound;
	}
	
	public synchronized void acquire() {
		try {
			while(this.signals == bound) {
				wait();
			}
			this.signals++;
			this.notify();
		} catch(InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public synchronized void release() {
		try {
			while(this.signals == 0) {
				wait();
			}
			this.signals--;
			this.notify();
		} catch(InterruptedException e) {
			e.printStackTrace();
		}
	}
}
